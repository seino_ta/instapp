class AddDetailInformationToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :website, :string
    add_column :users, :introduce, :string
    add_column :users, :telephone, :string
    add_column :users, :sex, :integer
  end
end
