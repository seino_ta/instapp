User.create!(name:  "Example User",
             username: "Example User's User Name",
             email: "example@railstutorial.org",
             password:              "foobar",
             password_confirmation: "foobar",
             introduce: 'Hellooooo my name is Example User',
             admin:     true,
             activated: true,
             activated_at: Time.zone.now)

99.times do |n|
  name  = Faker::Name.name
  username  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  introduce = 'Hellooooo my name is Example User'
  password = "password"
  User.create!(name:  name,
              username: username,
              email: email,
              introduce: introduce,
              password:              password,
              password_confirmation: password,
              activated: true,
              activated_at: Time.zone.now)
end

users = User.order(:created_at).take(6)
50.times do
  content = Faker::Lorem.sentence(5)
  picture = open("#{Rails.root}/db/fixtures/dummy.png")
  users.each { |user| user.microposts.create!(content: content, picture:picture) }
end